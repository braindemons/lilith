use std::{io::Cursor, mem::size_of};

use {
    crossbeam_channel::{Receiver, Sender},
    nalgebra::{Matrix3, Matrix4, Vector2},
    slotmap::SecondaryMap,
    wgpu::{
        read_spirv, Adapter, BackendBit, BlendDescriptor, Buffer, BufferUsage, Color,
        ColorStateDescriptor, ColorWrite, CommandEncoderDescriptor, CompareFunction, CullMode,
        DepthStencilStateDescriptor, Device, DeviceDescriptor, Extensions, Extent3d, FrontFace,
        IndexFormat, InputStepMode, Limits, LoadOp, PipelineLayoutDescriptor, PowerPreference,
        PresentMode, PrimitiveTopology, ProgrammableStageDescriptor, Queue,
        RasterizationStateDescriptor, RenderPassColorAttachmentDescriptor,
        RenderPassDepthStencilAttachmentDescriptor, RenderPassDescriptor, RenderPipeline,
        RenderPipelineDescriptor, RequestAdapterOptions, StencilStateFaceDescriptor, StoreOp,
        Surface, SwapChain, SwapChainDescriptor, TextureDescriptor, TextureDimension,
        TextureFormat, TextureUsage, TextureView, VertexAttributeDescriptor,
        VertexBufferDescriptor, VertexFormat,
    },
};

use crate::{
    CreateSurfaceCommand, SurfaceId, SurfaceRenderCommand, CreateMeshCommand, MeshId, Vertex,
};

pub(crate) enum Command {
    CreateSurface {
        surface_id: SurfaceId,
        command: CreateSurfaceCommand,
    },
    CreateMesh {
        mesh_id: MeshId,
        command: CreateMeshCommand,
    },
    SurfaceRender {
        command: SurfaceRenderCommand,
    },
    Stop,
}

pub(crate) fn run_worker(receiver: Receiver<Command>, sender: Sender<()>) {
    let mut worker = RendererWorker::new();

    loop {
        let command = receiver.recv().unwrap();

        match command {
            Command::CreateSurface {
                surface_id,
                command,
            } => worker.create_surface(surface_id, command),
            Command::CreateMesh { mesh_id, command } => worker.create_mesh(mesh_id, command),
            Command::SurfaceRender { command } => {
                worker.surface_render(command);
                sender.send(()).unwrap();
            }
            Command::Stop => return,
        }
    }
}

struct SurfaceEntry {
    surface: Surface,
    swap_chain: SwapChain,
    depth_view: TextureView,
    size: Vector2<u32>,
}

struct MeshEntry {
    vertex_buffer: Buffer,
    index_buffer: Buffer,
}

struct RendererWorker {
    device: Device,
    queue: Queue,

    surfaces_entries: SecondaryMap<SurfaceId, SurfaceEntry>,
    mesh_entries: SecondaryMap<MeshId, MeshEntry>,

    render_pipeline: RenderPipeline,
}

impl RendererWorker {
    #[allow(clippy::new_without_default)]
    fn new() -> Self {
        let adapter = Adapter::request(&RequestAdapterOptions {
            power_preference: PowerPreference::Default,
            backends: BackendBit::PRIMARY,
        })
        .unwrap();

        let (device, queue) = adapter.request_device(&DeviceDescriptor {
            extensions: Extensions {
                anisotropic_filtering: false,
            },
            limits: Limits::default(),
        });

        let render_pipeline = create_render_pipeline(&device);

        Self {
            device,
            queue,

            surfaces_entries: SecondaryMap::new(),
            mesh_entries: SecondaryMap::new(),

            render_pipeline,
        }
    }

    fn create_surface(&mut self, surface_id: SurfaceId, command: CreateSurfaceCommand) {
        let swap_chain = create_swap_chain(&self.device, &command.surface, command.size);
        let depth_view = create_depth_buffer(&self.device, command.size);

        self.surfaces_entries.insert(
            surface_id,
            SurfaceEntry {
                surface: command.surface,
                swap_chain,
                depth_view,
                size: command.size,
            },
        );
    }

    fn create_mesh(&mut self, mesh_id: MeshId, command: CreateMeshCommand) {
        let vertex_buffer = self
            .device
            .create_buffer_mapped(command.vertices.len(), BufferUsage::VERTEX)
            .fill_from_slice(&command.vertices);
        let index_buffer = self
            .device
            .create_buffer_mapped(command.indices.len(), BufferUsage::INDEX)
            .fill_from_slice(&command.indices);

        let entry = MeshEntry {
            vertex_buffer,
            index_buffer,
        };
        self.mesh_entries.insert(mesh_id, entry);
    }

    fn surface_render(&mut self, command: SurfaceRenderCommand) {
        // Don't render anything if there's 0 pixels to render
        if command.size.x == 0 || command.size.y == 0 {
            return;
        }

        let surface_entry = self.surfaces_entries.get_mut(command.surface_id).unwrap();

        // Resize the swap chain if the size changed
        if surface_entry.size != command.size {
            surface_entry.swap_chain =
                create_swap_chain(&self.device, &surface_entry.surface, command.size);
            surface_entry.depth_view = create_depth_buffer(&self.device, command.size);
            surface_entry.size = command.size;
        }

        let projection_view = create_projection_view_matrix(&command);

        // Calculate the matrices and put together a batches list
        let mut instances = Vec::new();
        let mut batches = Vec::new();
        for object in &command.render_command.objects {
            let total_matrix = projection_view * object.model_matrix;
            let normal_matrix = object
                .model_matrix
                .try_inverse()
                .unwrap()
                .transpose()
                .remove_column(3)
                .remove_row(3);

            let index = instances.len();
            instances.push(InstanceData {
                total_matrix,
                normal_matrix,
            });

            batches.push(ObjectBatch {
                instance_data_address: (index * size_of::<InstanceData>()) as u64,
                mesh_id: object.mesh,
            })
        }

        // Upload the instances
        let instances_buffer = self
            .device
            .create_buffer_mapped(instances.len(), BufferUsage::VERTEX)
            .fill_from_slice(&instances);

        let frame = surface_entry.swap_chain.get_next_texture();

        let mut encoder = self
            .device
            .create_command_encoder(&CommandEncoderDescriptor { todo: 0 });

        {
            let sky = Color {
                r: 0.005,
                g: 0.005,
                b: 0.006,
                a: 1.0,
            };

            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                color_attachments: &[RenderPassColorAttachmentDescriptor {
                    attachment: &frame.view,
                    resolve_target: None,
                    load_op: LoadOp::Clear,
                    store_op: StoreOp::Store,
                    clear_color: sky,
                }],
                depth_stencil_attachment: Some(RenderPassDepthStencilAttachmentDescriptor {
                    attachment: &surface_entry.depth_view,
                    depth_load_op: wgpu::LoadOp::Clear,
                    depth_store_op: wgpu::StoreOp::Store,
                    stencil_load_op: wgpu::LoadOp::Clear,
                    stencil_store_op: wgpu::StoreOp::Store,
                    // Reversed Z, clear with 0.0 instead of 1.0
                    clear_depth: 0.0,
                    clear_stencil: 0,
                }),
            });

            render_pass.set_pipeline(&self.render_pipeline);

            for batch in batches {
                let mesh_entry = self.mesh_entries.get(batch.mesh_id).unwrap();

                render_pass.set_vertex_buffers(
                    0,
                    &[
                        (&mesh_entry.vertex_buffer, 0),
                        (&instances_buffer, batch.instance_data_address),
                    ],
                );
                render_pass.set_index_buffer(&mesh_entry.index_buffer, 0);
                render_pass.draw_indexed(0..(6 * 6), 0, 0..1);
            }
        }

        self.queue.submit(&[encoder.finish()]);
    }
}

struct ObjectBatch {
    instance_data_address: u64,
    mesh_id: MeshId,
}

#[derive(Copy, Clone)]
#[repr(C)]
struct InstanceData {
    total_matrix: Matrix4<f32>,
    normal_matrix: Matrix3<f32>,
}

fn create_swap_chain(device: &Device, surface: &Surface, size: Vector2<u32>) -> SwapChain {
    device.create_swap_chain(
        surface,
        &SwapChainDescriptor {
            usage: TextureUsage::OUTPUT_ATTACHMENT,
            format: TextureFormat::Bgra8UnormSrgb,
            width: size.x,
            height: size.y,
            present_mode: PresentMode::Vsync,
        },
    )
}

fn create_render_pipeline(device: &Device) -> RenderPipeline {
    let vs = include_bytes!("../shader/spv/object.vert.spv");
    let vs_module = device.create_shader_module(&read_spirv(Cursor::new(&vs[..])).unwrap());

    let fs = include_bytes!("../shader/spv/object.frag.spv");
    let fs_module = device.create_shader_module(&read_spirv(Cursor::new(&fs[..])).unwrap());

    let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
        bind_group_layouts: &[],
    });

    device.create_render_pipeline(&RenderPipelineDescriptor {
        layout: &pipeline_layout,
        vertex_stage: ProgrammableStageDescriptor {
            module: &vs_module,
            entry_point: "main",
        },
        fragment_stage: Some(ProgrammableStageDescriptor {
            module: &fs_module,
            entry_point: "main",
        }),
        rasterization_state: Some(RasterizationStateDescriptor {
            front_face: FrontFace::Ccw,
            cull_mode: CullMode::Back,
            depth_bias: 0,
            depth_bias_slope_scale: 0.0,
            depth_bias_clamp: 0.0,
        }),
        primitive_topology: PrimitiveTopology::TriangleList,
        color_states: &[ColorStateDescriptor {
            format: TextureFormat::Bgra8UnormSrgb,
            color_blend: BlendDescriptor::REPLACE,
            alpha_blend: BlendDescriptor::REPLACE,
            write_mask: ColorWrite::ALL,
        }],
        depth_stencil_state: Some(DepthStencilStateDescriptor {
            format: TextureFormat::Depth32Float,
            depth_write_enabled: true,
            // Reversed Z, use Greater instead of Less
            depth_compare: CompareFunction::Greater,
            stencil_front: StencilStateFaceDescriptor::IGNORE,
            stencil_back: StencilStateFaceDescriptor::IGNORE,
            stencil_read_mask: 0,
            stencil_write_mask: 0,
        }),
        index_format: IndexFormat::Uint32,
        vertex_buffers: &[
            VertexBufferDescriptor {
                stride: size_of::<Vertex>() as u64,
                step_mode: InputStepMode::Vertex,
                attributes: &[
                    // Position
                    VertexAttributeDescriptor {
                        offset: 0,
                        format: VertexFormat::Float3,
                        shader_location: 0,
                    },
                    // Normal
                    VertexAttributeDescriptor {
                        offset: 4 * 3,
                        format: VertexFormat::Float3,
                        shader_location: 1,
                    },
                ],
            },
            VertexBufferDescriptor {
                stride: size_of::<InstanceData>() as u64,
                step_mode: InputStepMode::Instance,
                attributes: &[
                    // The total matrix, broken down into 4 vec4s
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float4,
                        offset: 0,
                        shader_location: 2,
                    },
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float4,
                        offset: 4 * 4,
                        shader_location: 3,
                    },
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float4,
                        offset: 4 * 4 * 2,
                        shader_location: 4,
                    },
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float4,
                        offset: 4 * 4 * 3,
                        shader_location: 5,
                    },
                    // The normal matrix, broken down into 4 vec4s
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float3,
                        offset: 4 * 4 * 4,
                        shader_location: 6,
                    },
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float3,
                        offset: (4 * 4 * 4) + (4 * 3),
                        shader_location: 7,
                    },
                    wgpu::VertexAttributeDescriptor {
                        format: wgpu::VertexFormat::Float3,
                        offset: (4 * 4 * 4) + (4 * 3 * 2),
                        shader_location: 8,
                    },
                ],
            },
        ],
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    })
}

fn create_depth_buffer(device: &Device, window_size: Vector2<u32>) -> TextureView {
    let depth_texture = device.create_texture(&TextureDescriptor {
        size: Extent3d {
            width: window_size.x,
            height: window_size.y,
            depth: 1,
        },
        array_layer_count: 1,
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: TextureFormat::Depth32Float,
        usage: TextureUsage::OUTPUT_ATTACHMENT,
    });

    depth_texture.create_default_view()
}

fn create_projection_view_matrix(command: &SurfaceRenderCommand) -> Matrix4<f32> {
    let projection = create_projection_matrix(command.size);
    let view = command
        .render_command
        .camera_model_matrix
        .try_inverse()
        .unwrap();

    projection * view
}

fn create_projection_matrix(framebuffer_size: Vector2<u32>) -> Matrix4<f32> {
    // We need FOV in radians
    let fov_deg = 90.0;
    let fov_h = fov_deg * std::f32::consts::PI / 180.0;

    // Calculate the matrix for the camera
    let mut projection: Matrix4<f32> = reversed_infinite_perspective_rh_zo(
        framebuffer_size.x as f32 / framebuffer_size.y as f32,
        fov_h_to_v(fov_h, framebuffer_size),
        0.1,
    );

    // Adjust for vulkan's y-down NDC space
    projection[(1, 1)] *= -1.0;

    projection
}

fn fov_h_to_v(fov_h: f32, framebuffer_size: Vector2<u32>) -> f32 {
    let aspect = framebuffer_size.y as f32 / framebuffer_size.x as f32;
    2.0 * ((fov_h / 2.0).tan() * aspect).atan()
}

// This is pending to be published by nalgebra-glm
pub fn reversed_infinite_perspective_rh_zo(aspect: f32, fovy: f32, near: f32) -> Matrix4<f32> {
    let f = 1.0 / (fovy * 0.5).tan();
    let mut mat = Matrix4::zeros();

    mat[(0, 0)] = f / aspect;
    mat[(1, 1)] = f;
    mat[(2, 3)] = near;
    mat[(3, 2)] = -1.0;

    mat
}
