use std::thread::{spawn, JoinHandle};

use {
    crossbeam_channel::{unbounded, Receiver, Sender},
    nalgebra::Vector2,
    raw_window_handle::HasRawWindowHandle,
    slotmap::SlotMap,
    wgpu::Surface,
};

use crate::{
    worker::{run_worker, Command},
    CreateSurfaceCommand, SurfaceId, SurfaceRenderCommand,
    CreateMeshCommand, MeshId,
};

/// Host and interface to the rendering worker thread.
pub struct Renderer {
    handle: Option<JoinHandle<()>>,
    sender: Sender<Command>,
    receiver: Receiver<()>,
    is_render_pending: bool,

    surfaces: SlotMap<SurfaceId, ()>,
    meshes: SlotMap<MeshId, ()>,
}

impl Renderer {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        let (sender, worker_receiver) = unbounded();
        let (worker_sender, receiver) = unbounded();

        let handle = spawn(move || {
            run_worker(worker_receiver, worker_sender);
        });

        Self {
            handle: Some(handle),
            sender,
            receiver,
            is_render_pending: false,

            surfaces: SlotMap::with_key(),
            meshes: SlotMap::with_key(),
        }
    }

    pub fn create_surface<H: HasRawWindowHandle>(
        &mut self,
        window: &H,
        size: Vector2<u32>,
    ) -> SurfaceId {
        let surface = Surface::create(window);

        let surface_id = self.surfaces.insert(());

        let command = CreateSurfaceCommand { surface, size };

        self.sender
            .send(Command::CreateSurface {
                surface_id,
                command,
            })
            .unwrap();

        surface_id
    }

    pub fn create_mesh(&mut self, command: CreateMeshCommand) -> MeshId {
        let mesh_id = self.meshes.insert(());
        self.sender
            .send(Command::CreateMesh { mesh_id, command })
            .unwrap();

        mesh_id
    }

    pub fn surface_render(&mut self, command: SurfaceRenderCommand, blocking: bool) {
        // Send the command in advance, we'll wait for the last frame either way, and this way the
        // command will be picked up as soon as possible.
        self.sender
            .send(Command::SurfaceRender { command })
            .unwrap();

        // If there's still a previous surface render pending, wait on that being done before 
        // returning back to the caller.
        // This prevents render commands from accumulating when the render thread is slower.
        if self.is_render_pending {
            self.receiver.recv().unwrap();
            self.is_render_pending = false;
        }

        if blocking {
            self.receiver.recv().unwrap();
        } else {
            self.is_render_pending = true;
        }
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        if self.is_render_pending {
            let _ = self.receiver.recv();
        }

        let _ = self.sender.send(Command::Stop);
        let _ = self.handle.take().unwrap().join();
    }
}
