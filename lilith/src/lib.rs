#![deny(bare_trait_objects)]
#![warn(clippy::all)]

mod renderer;
mod worker;

pub use renderer::Renderer;

use {
    nalgebra::{Matrix4, Point3, Vector2, Vector3},
    slotmap::new_key_type,
    wgpu::Surface,
};

new_key_type! {
    pub struct SurfaceId;
    pub struct MeshId;
}

struct CreateSurfaceCommand {
    pub surface: Surface,
    pub size: Vector2<u32>,
}

pub struct SurfaceRenderCommand {
    pub surface_id: SurfaceId,
    pub size: Vector2<u32>,
    pub render_command: RenderCommand,
}

pub struct RenderCommand {
    pub camera_model_matrix: Matrix4<f32>,
    pub objects: Vec<ObjectCommand>,
}

pub struct ObjectCommand {
    pub model_matrix: Matrix4<f32>,
    pub mesh: MeshId,
}

pub struct CreateMeshCommand {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vertex {
    pub position: Point3<f32>,
    pub normal: Vector3<f32>,
}
