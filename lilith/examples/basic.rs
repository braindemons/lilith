use {
    nalgebra::Vector2,
    winit::{
        event::{Event, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
    },
};

use lilith::{RenderCommand, Renderer, SurfaceRenderCommand};

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    window.set_resizable(false);

    let size = window.inner_size();
    let size = size.to_physical(window.hidpi_factor());
    let size = Vector2::new(size.width as u32, size.height as u32);

    // Create the renderer with a surface for the window
    let mut renderer = Renderer::new();
    let surface_id = renderer.create_surface(&window, size);

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            Event::WindowEvent {
                event: WindowEvent::RedrawRequested,
                ..
            } => {
                // Render to the surface
                renderer.surface_render(
                    SurfaceRenderCommand {
                        surface_id,
                        size,
                        render_command: RenderCommand {
                            camera_model_matrix: nalgebra::one(),
                            objects: Vec::new(),
                        },
                    },
                    false,
                );
            }
            Event::EventsCleared => {
                window.request_redraw();
            }
            _ => {}
        }
    });
}
