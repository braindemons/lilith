#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;

// Per-instance
// vec4[4] is used instead of mat4 due to spirv-cross bug for dx12 backend
layout(location = 2) in vec4 i_total_matrix[4];
layout(location = 6) in vec3 i_normal_matrix[3];

layout(location = 0) out vec3 f_normal;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    mat4 total_matrix = mat4(
        i_total_matrix[0],
        i_total_matrix[1],
        i_total_matrix[2],
        i_total_matrix[3]
    );
    mat3 normal_matrix = mat3(
        i_normal_matrix[0],
        i_normal_matrix[1],
        i_normal_matrix[2]
    );

    f_normal = normalize(normal_matrix * v_normal);

    gl_Position = total_matrix * vec4(v_position, 1.0);
}
