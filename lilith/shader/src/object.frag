#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(early_fragment_tests) in;

layout(location = 0) in vec3 f_normal;

layout(location = 0) out vec4 o_target;

void main() {
    vec3 color = vec3(1.0, 1.0, 1.0);
    vec3 normal = normalize(f_normal);

    vec3 light_direction = normalize(vec3(-0.3, -0.5, -0.2));
    vec3 negative_light_direction = -light_direction;

    float ambient = 0.1;
    float light_strenght = 0.9;

    // Calculate the diffuse light contribution
    float aligned = max(dot(normal, negative_light_direction), 0.0);
    float diffuse = aligned * light_strenght;

    // Calculate the final light and color value
    float light = ambient + diffuse;
    vec3 lit_color = color * light;

    o_target = vec4(lit_color, 1.0);
}
